/**
 * Created by Acer on 24.06.2018.
 */
$(document).ready(function(){
    //хедер
    $(window).scroll(function(){
        var x = $(window).scrollTop();
        if(x == 0){
            $('.header').removeClass('header__scroll');
        }else{
            $('.header').addClass('header__scroll');
        }

        if(x >= 400){
            $('.header').addClass('header__button_show');
        }else{
            $('.header').removeClass('header__button_show');
        }
    });

    //слайдер по шапкой
    $('.main__slider_slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 10000
    });

    //Слайдер в блоках "Им нужна помощь", "Они ищут дом"
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.slider-nav',
        adaptiveHeight: true
    });
    $('.main__need-help_direct.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        asNavFor: '.slider-for',
        focusOnSelect: true
    });

    $('.slider-to').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.slider-on',
        adaptiveHeight: true
    });
    $('.slider-on').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-to',
        focusOnSelect: true
    });
});