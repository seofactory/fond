/**
 * Created by Acer on 24.06.2018.
 */

function map_inizialization(){
    ymaps.ready(init);
    var myMap, placemarks = [];


    function init() {
        var animals = [];
        $.ajax({
            method: 'get',
            dataType: 'json',
            async: false,
            url: '/api/?get-address-animals=1',
            success: function(response){
                if(response){
                    response.forEach(function(elem){
                        animals.push(elem);
                    });
                }
            }
        });
        myMap = new ymaps.Map("map", {
            center: [55.75399399999374, 37.62209300000001],
            zoom: 10,
            controls: ["zoomControl"]
        });
        //myMap.behaviors.disable('drag');
        myMap.behaviors.disable('scrollZoom');

        var customBalloonContentLayout = ymaps.templateLayoutFactory.createClass([
            '{% for geoObject in properties.geoObjects %}',
            '{{ geoObject.properties.hintContent|raw }}',
            '{% endfor %}',
        ].join(''));

        var ClusterHintLayout = ymaps.templateLayoutFactory.createClass(
            '<div class="hint">' +
            '<h4>Джек</h4>' +
            '<p>Сбор на лечение</p>' +
            '</div>'+
            '<div class="hint">' +
            '<h4>Том</h4>' +
            '<p>Сбор на лечение</p>' +
            '</div>');

        clusterer = new ymaps.Clusterer({
            groupByCoordinates: false,
            clusterOpenBalloonOnClick: false,
            clusterDisableClickZoom: true,
            clusterHideIconOnBalloonOpen: false,
            geoObjectHideIconOnBalloonOpen: false,
            clusterHintContentLayout: customBalloonContentLayout,
            clusterIcons: [{
                href: '/web/images/map_icon.png',
                size: [26, 33],
                offset: [-15, -50]
            }],
            hintPane: 'hint'
        });

        clusterer.options.set({
            gridSize: 50,
            clusterDisableClickZoom: true
        });
        animals.forEach(function(elem){
            var coords = elem.coords.split(',');
            placemarks.push(new ymaps.Placemark([coords[0],coords[1]], { hintContent: '<div class="hint"><h4>'+elem.name+'</h4><p>'+elem.help+'</p></div>', placemarkId: 1}, { iconLayout: 'default#image', iconImageHref: '/web/images/map_icon.png', iconImageSize: [26, 33], iconImageOffset: [-15, -50], hintContentSize:[300,100], hintPane: 'hint'}));
        });

        clusterer.add(placemarks);
        myMap.geoObjects.add(clusterer);
    }
}

$(document).ready(function(){
    if($('#map').length != 0) {
        map_inizialization();
    }
});
