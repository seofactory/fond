(function(e){function z(a){var c=e(a),b=k(c),d=b.data("maxFiles"),f=b.data("fieldName"),g=function(a){return"multifile_"+a+"["+f+"][]"},t=r(a);c.hide().clone().val("").appendTo(b).show();c.removeClass("nc-upload-input").appendTo(b.find(".nc-upload-new-files"));e.each(t,function(f){var h=b.data("nextUploadIndex")||0;b.data("nextUploadIndex",h+1);1==t.length&&c.addClass("nc-upload-file-index--"+h);f=u(a,f).hide().data("uploadIndex",h).append(l(g("id")),l(g("upload_index"),h),l(g("delete"),0).addClass("nc-upload-file-remove-hidden"));
b.data("customName")&&(h=b.data("customNameCaption"),h=v("text",g("name"),"",h),e('<div class="nc-upload-file-custom-name"/>').append(h).appendTo(f));d&&"0"!=d&&b.find(".nc-upload-file:visible").length>=d?w(f):f.slideDown(100)});m(b)}function u(a,c){var b=r(a)[c],d=e('<div class="nc-upload-file"><div class="nc-upload-file-info"><span class="nc-upload-file-name">'+b.name+'</span> <a href="#" class="nc-upload-file-remove" tabindex="-1">\u00d7</a></div></div>'),f=k(a).find(".nc-upload-files");if(n&&
5242880>b.size&&x(b.type)){var g=new FileReader;g.onload=function(a){p(d,b.type,b.name,a.target.result,!0)};g.readAsDataURL(b)}else p(d,b.type,b.name,null,!0);d.appendTo(f);y(d);return d}function p(a,c,b,d,f){var g=e("<div/>").addClass("nc-upload-file-preview nc-upload-file-drag-handle");(c=d&&x(c))&&n?(g.addClass("nc-upload-file-preview-image").append('<img src="'+d+'" />'),k(a).addClass("nc-upload-with-preview")):(d=b.lastIndexOf("."),g.addClass("nc-upload-file-preview-other"),0<d&&g.append('<span class="nc-upload-file-extension">'+
b.substr(d+1)+"</span>"));a.prepend('<div class="nc-upload-file-drag-icon nc-upload-file-drag-handle"><i class="nc-icon nc--file-text"></i></div>',g);(c||a.closest(".nc-upload-with-preview").length)&&n&&(f?g.slideDown(100):g.show());return g}function x(a){return /^image\/(jpe?g|png|gif|bmp|svg([+-]xml)?)$/i.test(a)}function r(a){return n?a.files:[{name:a.value.substr(a.value.lastIndexOf("\\")+1),type:"",size:0}]}function q(a){return a.hasClass("nc-upload-multifile")}function k(a){return e(a).closest(".nc-upload")}
function m(a){var c=q(a)?a.data("maxFiles"):1;c&&a.find(".nc-upload-input").toggle(a.find(".nc-upload-file:visible").length<c)}function l(a,c){return v("hidden",a,c)}function v(a,c,b,d){return e("<input />",{type:a,name:c,value:void 0===b?"":b,placeholder:d||""})}function A(a){a.preventDefault();a.stopPropagation();a=e(a.target).closest(".nc-upload-file");var c=k(a),b=c.find(".nc-upload-input"),d=b;w(a);q(c)?c.find(".nc-upload-file-index--"+a.data("uploadIndex")).val(""):(d=b.clone().val("").show(),
b.replaceWith(d));a.slideUp(100,function(){c.hasClass("nc-upload-with-preview")&&0==c.find(".nc-upload-file:visible .nc-upload-file-preview-image").length&&c.removeClass("nc-upload-with-preview");var a=e.Event("change");a.target=d[0];e(document).trigger(a);m(c)});return!1}function w(a){a.find(".nc-upload-file-remove-hidden").val(1)}function y(a){a.find(".nc-upload-file-remove").off("click.nc-upload").on("click.nc-upload",A)}if(e){var n="FileReader"in window;e.fn.upload=function(){return this.each(function(){var a=
e(this);if(!a.hasClass("nc-upload-applied")){a.addClass("nc-upload-applied");var c=q(a),b=a.find(".nc-upload-files"),d=b.find(".nc-upload-file");0<d.length&&!c&&a.find(".nc-upload-input").hide();d.each(function(){var a=e(this),b=a.data("type"),c=a.find(".nc-upload-file-name"),d=c.html(),c=c.prop("href");p(a,b,d,c,!1)});setTimeout(function(){m(a)},10);a.on("change",".nc-upload-input",function(){c?z(this):this.value&&(u(this,0).hide().slideDown(100).find(".nc-upload-file-custom-name input:text").focus(),
m(k(this)))});b.find(".nc-upload-file-remove").attr("onclick","");y(b);c&&a.append(l("multifile_js["+a.data("fieldName")+"]",1));if(c){var f;b.on("mousedown",".nc-upload-file-drag-handle",function(a){a.preventDefault();b.addClass("nc--dragging");f=e(this).closest(".nc-upload-file").addClass("nc--dragged");e(window).on("mouseup.nc-upload",function(){b.removeClass("nc--dragging");f.removeClass("nc--dragged");e(window).off("mouseup.nc-upload");f=null})});b.on("mousemove",".nc-upload-file",function(a){var b=
e(this);if(f&&!b.hasClass("nc--dragged")){if("none"==b.css("float")){a=a.pageY-b.offset().top;var c=b.height()}else a=a.pageX-b.offset().left,c=b.width();(a=a<c/2)?f.insertBefore(b):f.insertAfter(b)}})}}})};e(document).on("apply-upload",function(a){e(".nc-upload").upload()})}})(window.$nc||window.jQuery);

if (typeof(lsDisplayLibLoaded) == 'undefined') {
    var lsDisplayLibLoaded = true;
    var E_CLICK  = 0,
        E_SUBMIT = 1;

    jQuery(function(){
        var bindEvents = function($container){
            jQuery('[data-nc-ls-display-link]', $container).click(function(){
                eventHandler(this, true, E_CLICK);
                return false;
            });
            jQuery('form[data-nc-ls-display-form]', $container).submit(function(){
                eventHandler(this, true, E_SUBMIT);
                return false;
            });
        }

        var eventHandler = function(element, callBindEvents, event_type){

            switch (event_type) {

                case E_SUBMIT:
                    var url_attr  = 'action';
                    var data_attr = 'data-nc-ls-display-form';
                    break;

                case E_CLICK:
                default:
                    var url_attr  = 'href';
                    var data_attr = 'data-nc-ls-display-link';
                    break;
            }

            var $this    = jQuery(element);
            var url      = $this.attr(url_attr);
            var obj_data = $this.attr(data_attr);

            if (obj_data) {
                obj_data = jQuery.parseJSON(obj_data);
            }
            else {
                return false;
            }

            var replace_content = obj_data.subdivisionId !== false;

            if (url) {
                if (obj_data.displayType == 'shortpage' || (obj_data.displayType == 'longpage_vertical' && typeof(obj_data.subdivisionId) == 'undefined')) {

                    var send_as_post = event_type === E_SUBMIT && $this.attr('method').toLowerCase() === 'post';
                    var send_data    = jQuery.extend({}, obj_data.query); // clone

                    send_data.isNaked       = parseInt(typeof send_data.isNaked !== 'undefined' ? send_data.isNaked : 1);
                    send_data.lsDisplayType = obj_data.displayType;
                    send_data.skipTemplate  = parseInt(send_data.skipTemplate ? send_data.skipTemplate : obj_data.displayType == 'shortpage' && typeof(obj_data.subdivisionId) != 'undefined' ? 1 : 0);

                    if (send_as_post) {
                        url += (url.indexOf('?') >= 0 ? '&' : '?') + jQuery.param(send_data);
                        send_data = $this.serialize();
                    }

                    jQuery.ajax({
                        type:    send_as_post ? 'POST' : 'GET',
                        url:     url,
                        data:    send_data,
                        success: function(data){
                            var $container = [];

                            if (typeof(obj_data.onSubmit) !== 'undefined') {
                                if (data[0] == '{' || data[0] == '[') {
                                    data = jQuery.parseJSON(data);
                                }

                                if ((eval(obj_data.onSubmit)).call($this.get(0), data) === false) {
                                    replace_content = false;
                                }
                            }

                            if ( ! replace_content) {
                                return false;
                            }

                            if (typeof(obj_data.subdivisionId) == 'undefined') {
                                $container = $this.closest('[data-nc-ls-display-container]');
                            } else {
                                jQuery('[data-nc-ls-display-container]').each(function(){
                                    var $element = jQuery(this);
                                    var containerData = $element.attr('data-nc-ls-display-container');
                                    if (containerData) {
                                        containerData = jQuery.parseJSON(containerData);
                                        if (containerData.subdivisionId == obj_data.subdivisionId) {
                                            $container = $element;
                                            return false;
                                        }
                                    }

                                    return true;
                                });
                            }

                            if (!$container.length) {
                                $container = jQuery('[data-nc-ls-display-container]');
                            }

                            $container.html(data);

                            if (callBindEvents) {
                                bindEvents($container);
                            }

                            if (typeof(parent.nc_ls_quickbar) != 'undefined') {
                                var quickbar = parent.nc_ls_quickbar;
                                if (quickbar) {
                                    var $quickbar = jQuery('.nc-navbar').first();
                                    $quickbar.find('.nc-quick-menu LI:eq(0) A').attr('href', quickbar.view_link);
                                    $quickbar.find('.nc-quick-menu LI:eq(1) A').attr('href', quickbar.edit_link);
                                    $quickbar.find('.nc-menu UL LI:eq(0) A').attr('href', quickbar.sub_admin_link);
                                    $quickbar.find('.nc-menu UL LI:eq(1) A').attr('href', quickbar.template_admin_link);
                                    $quickbar.find('.nc-menu UL LI:eq(2) A').attr('href', quickbar.admin_link);
                                }
                            }
                        }
                    });

                } else if (obj_data.displayType == 'longpage_vertical') {
                    var scrolled = false;

                    var scrollToContainer = function(containerData, $element){
                        if (containerData) {
                            containerData = jQuery.parseJSON(containerData);
                            if (containerData.subdivisionId == obj_data.subdivisionId) {
                                jQuery('HTML,BODY').animate({
                                    scrollTop: $element.offset().top - jQuery('BODY').offset().top
                                }, containerData.animationSpeed);
                                return true;
                            }
                        }

                        return false;
                    };

                    jQuery('[data-nc-ls-display-pointer]').each(function(){
                        var $element = jQuery(this);
                        if (scrollToContainer($element.attr('data-nc-ls-display-pointer'), $element)) {
                            scrolled = true;
                            return false;
                        }

                        return true;
                    });

                    if (!scrolled) {
                        jQuery('[data-nc-ls-display-container]').each(function(){
                            var $element = jQuery(this);

                            if (scrollToContainer($element.attr('data-nc-ls-display-container'), $element)) {
                                return false;
                            }

                            return true;
                        });
                    }
                }

                if (replace_content) {
                    if (!!(window.history && history.pushState)) {
                        window.history.pushState({}, '', url);
                    }
                }

                if (event_type === E_CLICK) {
                    if (typeof(obj_data.onClick) == 'undefined') {
                        $this.addClass('active').siblings().removeClass('active');
                    } else {
                        eval('var callback = ' + obj_data.onClick);
                        callback.call($this.get(0));
                    }
                }

                return false;
            }
        }

        jQuery('[data-nc-ls-display-link]').click(function(){
            eventHandler(this, true, E_CLICK);
            return false;
        });

        jQuery('form[data-nc-ls-display-form]').submit(function(){
            eventHandler(this, true, E_SUBMIT);
            return false;
        });

        jQuery('[data-nc-ls-display-pointer]').each(function(){
            var $this = jQuery(this);
            var data = jQuery.parseJSON($this.attr('data-nc-ls-display-pointer'));
            if (data.onReadyScroll) {
                setTimeout(function(){
                    jQuery('HTML,BODY').scrollTop($this.offset().top);
                }, 1000);
                return false;
            }

            return true;
        });
    });
}
